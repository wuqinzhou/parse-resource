﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace PerformanceTest
{
	public class TestSystemAPI:TestBase
	{
		public TestSystemAPI ()
		{
		}

		protected override string TestAPI (string strRootPath, List<string> lstFile)
		{
			// 获得计时器
			Stopwatch timer = new Stopwatch();

            long nTotalBytes = 0;

			// 开始计时
			timer.Start ();

			// 遍历所有文件
			foreach (string strFileName in lstFile) 
			{
				// 只读方式打开
				using (FileStream fsFile = File.OpenRead (strRootPath + "/" + strFileName)) 
				{
					byte[] byData = new byte[fsFile.Length];
                    nTotalBytes += byData.Length;

					// 读取文件数据
					fsFile.Read (byData, 0, byData.Length);

					fsFile.Close ();
				}
		
			}

			// 停止计时
			timer.Stop ();

			return string.Format ("SystemAPI 读取 {0:D} 个文件,总共{1:D}M，耗时{2:D}毫秒", 
                lstFile.Count, 
                nTotalBytes/1024/1024, 
                timer.ElapsedMilliseconds);
		}
	}
}

