﻿using System;
using System.Collections.Generic;

namespace PerformanceTest
{
	/// <summary>
	/// 测试基类。决定测试流程
	/// </summary>
	public abstract class TestBase
	{
		public TestBase ()
		{
		}

		// 开始测试
		public void StartTest (string strRootPath, List<string> lstFile)
		{
			// 开始测试前手动进行垃圾回收，尽量排除内存消耗对测试结果的影响
			GC.Collect();

			ProcessResult(TestAPI(strRootPath, lstFile));
		}

		// 进行测试，子类必须重载
		protected abstract string TestAPI (string strRootPath, List<string> lstFile);

		// 处理测试结果，子类可重载
		protected virtual void ProcessResult (string strResult)
		{
			Console.WriteLine (strResult);
		}

	}
}

