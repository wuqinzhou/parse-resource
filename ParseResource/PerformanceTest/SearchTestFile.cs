﻿using System;
using System.Collections.Generic;
using ParseResource;

namespace PerformanceTest
{
	// 测试类型
	public enum EPerformanceTestType
	{
		// 测资源包前半部分
		ePTT_FormerFile		= 1,

		// 测资源包后半部分
		ePPT_LatterFile		= 2,

		// 较小文件
		ePPT_SmallerFile	= 3,

		// 较大文件
		ePPT_LargeFile		= 4,

		// 所有文件
		ePPT_AllFile		= 5,
	}
		
	/// <summary>
	/// 查找文件类。根据传入的参数输出要测试的文件
	/// </summary>
	public class SearchTestFile
	{
		// 单例
		private static SearchTestFile inst;
		public static SearchTestFile Inst
		{
			get 
			{
				if (inst == null) 
				{
					inst = new SearchTestFile ();
				}
				return inst;
			}
		}

		public SearchTestFile ()
		{
		}

		// 开始测试
		// eTestType 测试类型
		// dMultiple 测试的文件数量倍数。比如一个资源包有1000个文件，选择测试前半部分，就是测前面500个文件，倍数为3就是测1500次
		// bRepeat 测试的文件是否可以重复
		public List<string> SearchFile(EPerformanceTestType eTestType, double dMultiple, bool bRepeat = false)
		{
			// 获得所有资源包中的所有文件
			List<tagFileObj> lstFileObj = ResourceMgr.Inst.GetLoadResourceData ();

			// 列表排序
			if(EPerformanceTestType.ePTT_FormerFile == eTestType ||
				EPerformanceTestType.ePPT_LatterFile == eTestType)
			{
				// 文件按在资源中的位置排序
				lstFileObj.Sort ((left, right) => 
					{
						if(left.nBegin == right.nBegin)
						{
							return 0;
						}
						return left.nBegin > right.nBegin ? 1 : -1;				
					});
				
			}
			else if(EPerformanceTestType.ePPT_SmallerFile == eTestType ||
				EPerformanceTestType.ePPT_LargeFile == eTestType )
			{
				// 文件按大小排序
				lstFileObj.Sort ((left, right) => 
					{
						if(left.nLength == right.nLength)
						{
							return 0;
						}
						return left.nLength > right.nLength ? 1 : -1;				
					});
			}

			// 去掉列表的后一半
			if (EPerformanceTestType.ePTT_FormerFile == eTestType ||
			    EPerformanceTestType.ePPT_SmallerFile == eTestType)
			{
				lstFileObj.RemoveRange (lstFileObj.Count / 2, lstFileObj.Count - lstFileObj.Count / 2);
			} 
			// 去掉列表的前一半
			else if (EPerformanceTestType.ePPT_LatterFile == eTestType ||
			        EPerformanceTestType.ePPT_LargeFile == eTestType)
			{
				lstFileObj.RemoveRange (0, lstFileObj.Count / 2);
			}

			// 如果不重复，最多只能是每个文件读取一遍
			if (!bRepeat && dMultiple > 1.0)
			{
				dMultiple = 1.0;
			}

			List<string> lstFileName = new List<string> ();
			Random rdIndex = new Random ();

			int nFileNum = (int)(lstFileObj.Count * dMultiple);

			// 添加一定数量的文件名
			for (int i = 0; i < nFileNum; i++)
			{
				// 随机生成下标
				int nIndex = rdIndex.Next (0, lstFileObj.Count - 1);

				lstFileName.Add (lstFileObj [nIndex].strFileName);

				// 如果不重复，就把加过的文件名移除
				if (!bRepeat)
				{
					lstFileObj.RemoveAt (nIndex);
				}
			}
						
			return lstFileName;
		}

	}
}

