﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using ParseResource;

namespace PerformanceTest
{
	/// <summary>
	/// 测试自定义的API
	/// </summary>
	public class TestUserDefinedAPI:TestBase
	{
		public TestUserDefinedAPI ()
		{
		}

		protected override string TestAPI (string strRootPath, List<string> lstFile)
		{
			// 获得计时器
			Stopwatch timer = new Stopwatch();

            long nTotalBytes = 0;

			// 开始计时
			timer.Start ();

			// 读取所有文件
			foreach (string strFileName in lstFile) 
			{
				byte[] byData = ResourceMgr.Inst.GetSrcData (strFileName);
                nTotalBytes += byData.Length;
			}

			// 停止计时
			timer.Stop ();

            return string.Format("UserDefinedAPI 读取 {0:D} 个文件,总共{1:D}M，耗时{2:D}毫秒", 
                lstFile.Count, 
                nTotalBytes/1024/1024, 
                timer.ElapsedMilliseconds);		
        }
	}
}

