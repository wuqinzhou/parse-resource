﻿using System;
using System.Collections.Generic;
using ParseResource;

namespace PerformanceTest
{	
	class MainClass
	{
		public static void Main (string[] args)
		{
			Console.WriteLine("请输入资源根目录");

			string strRootPath = Console.ReadLine ();

			// 加载资源/Users/macbookpro/Desktop
			if(!ResourceMgr.Inst.Loading(strRootPath))
			{
				return;
			}

			Console.WriteLine ("请输入测试类型:\n1.资源包前半部分\n2.资源包后半部分\n3.较小文件\n4.较大文件\n5.所有文件");

			int nType = Convert.ToInt32 (Console.ReadLine ());

			Console.WriteLine ("请输入测试的倍数:");

			double dMultiple = Convert.ToDouble (Console.ReadLine ());

			Console.WriteLine ("测试文件是否可以重复：");

			bool bRepeat = Convert.ToBoolean (Console.ReadLine ());

			List<string> lstFileName = SearchTestFile.Inst.SearchFile ((EPerformanceTestType)nType, dMultiple, bRepeat);

			// 测试系统API
			TestBase tSysAPI = new TestSystemAPI ();
			tSysAPI.StartTest (strRootPath, lstFileName);

			// 测试用户API
			TestBase tUserAPI = new TestUserDefinedAPI ();
			tUserAPI.StartTest (strRootPath, lstFileName);

            Console.Read();
		}


	
	}
}
