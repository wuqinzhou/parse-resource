﻿using System;

namespace ParseResource
{
	/// <summary>
	/// 通用适配器，返回字节流数据
	/// </summary>
	public class CommonFileAdaptor:FileAdaptor
	{
		// 单例
		private static CommonFileAdaptor inst;
		public static CommonFileAdaptor Inst
		{
			get 
			{
				lock (SynObject)
				{
					if (inst == null) 
					{
						inst = new CommonFileAdaptor ();
					}
					return inst;
				}
			}
		}

		public CommonFileAdaptor ()
		{
		}

		public override object GetObject(string strFileName, int nVersion)
		{
			byte[] byData = ResourceData.Inst.GetBytesData (strFileName, nVersion);

			return byData;
		}

		public byte[] GetBytes(string strFileName, int nStartIndex, int nLength)
		{
			return ResourceData.Inst.GetBytesData (strFileName, nStartIndex, nLength);
		}
	}
}

