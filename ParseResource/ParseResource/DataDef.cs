﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections;

namespace ParseResource
{
	/// <summary>
	/// 文件对象，存储单个文件的数据
	/// </summary>
	public struct tagFileObj
	{
		// 文件名
		public string strFileName;
		// 文件长度
		public Int32 nLength;
		// 文件内容在资源包的开始位置
		public Int64 nBegin;
		// 所属资源包的文件流
		public FileStream fsResource;
		// 所属资源包名
		public string strResName;
	}

	public struct tagFileKey
	{
		public string strFileName;
		public int nVersion;

		public tagFileKey(string strName, int nVer)
		{
			strFileName = strName;
			nVersion = nVer;
		}
	}

	public class FileKeyComparer : IEqualityComparer<tagFileKey>
	{
		public int GetHashCode(tagFileKey key)
		{
			return (key.strFileName + key.nVersion.ToString ()).GetHashCode ();
		}

		public bool Equals(tagFileKey lhs, tagFileKey rhs)
		{
			return lhs.strFileName == rhs.strFileName && lhs.nVersion == rhs.nVersion;
		}
	}
}

