﻿using System;
using System.Text;

namespace ParseResource
{
	/// <summary>
	/// 文本适配器，适合文本资源
	/// </summary>
	public class TextAdaptor:FileAdaptor
	{
		// 单例
		private static TextAdaptor inst;
		public static TextAdaptor Inst
		{
			get 
			{
				lock (SynObject)
				{
					if (inst == null) 
					{
						inst = new TextAdaptor ();
					}
					return inst;
				}
			}
		}

		public TextAdaptor ()
		{
		}

		public override object GetObject(string strFileName, int nVersion)
		{
			byte[] byData = ResourceData.Inst.GetBytesData (strFileName, nVersion);

			// 判空
			if (byData == null) 
			{
				return null;
			}

			return Encoding.UTF8.GetString(byData);
		}
	}
}

