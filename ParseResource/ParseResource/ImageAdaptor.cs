﻿using System;
using System.IO;
using System.Drawing;

namespace ParseResource
{
	/// <summary>
	/// 图片适配器，返回图片资源数据
	/// </summary>
	public class ImageAdaptor:FileAdaptor
	{
		// 单例
		private static ImageAdaptor inst;
		public static ImageAdaptor Inst 
		{
			get 
			{
				lock (SynObject)
				{
					if (inst == null) 
					{
						inst = new ImageAdaptor ();
					}
					return inst;
				}
			}
		}

		public ImageAdaptor ()
		{
		}

		public override object GetObject(string strFileName, int nVersion)
		{
			byte[] byData = ResourceData.Inst.GetBytesData (strFileName, nVersion);

			// 判空
			if (byData == null) 
			{
				return null;
			}

			MemoryStream msData = new MemoryStream(byData);
			Image imgData = Image.FromStream(msData);
			return imgData;
		}
	}
}

