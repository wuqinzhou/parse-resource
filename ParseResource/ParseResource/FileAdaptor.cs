﻿using System;

namespace ParseResource
{
	/// <summary>
	/// 文件适配器基类
	/// 将得到的字节数组转化为具体的对象
	/// </summary>
	public abstract class FileAdaptor
	{
		// 保证单例为线程安全的辅助对象
		protected static readonly object SynObject = new object();

		public FileAdaptor ()
		{
		}

		public abstract object GetObject(string strFileName, int nVersion);
	}
}

