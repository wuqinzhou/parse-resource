﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace ParseResource
{
	/// <summary>
	/// 资源数据类
	/// 解析资源，并提供获取文件字节数组的接口
	/// </summary>
	public class ResourceData
	{
		// 资源包对应的数据
		private Dictionary<tagFileKey, tagFileObj> m_dtFileData;

		// 资源文件数据，有序
		private List<tagFileObj> m_lstFileObj;

		// 资源包的加载顺序
		private Dictionary<string, int> m_dtResLoadOrder;

		// 资源包的加载情况
		private HashSet<string> m_setLoadedRes;

		// 保证单例为线程安全的锁的对象
		private static readonly object SynInst = new object();

		// 字典集合多线程访问的锁的对象
		private static readonly object SynDtRes = new object();

		// 资源包的加载情况多线程访问的锁的对象
		private static readonly object SynLoadState = new object ();

		// 资源的加载ID
		private static int nLoadID = 0;

		// 单例
		private static ResourceData inst;
		public static ResourceData Inst 
		{
			get {
				lock (SynInst)
				{
					if (inst == null) 
					{
						inst = new ResourceData ();
					}
					return inst;
				}
			}
		}

		public ResourceData ()
		{
			m_dtFileData = new Dictionary<tagFileKey, tagFileObj> ();
			m_dtResLoadOrder = new Dictionary<string, int> ();
			m_setLoadedRes = new HashSet<string> ();
			m_lstFileObj = new List<tagFileObj> ();
		}

		~ResourceData()
		{
			Release ();
		}

		// 开始加载一个资源
		public void StartLoadRes(string strResName)
		{
			// 分配资源包加载ID
			m_dtResLoadOrder.Add (strResName, nLoadID++);

			// 利用线程池分配线程加载资源
			// 每个资源都由一个子线程加载，保证主线程不因加载资源而堵塞
			ThreadPool.QueueUserWorkItem (ResourceData.Inst.Load, strResName);
		}

		// 加载资源，传和资源名（含路径）
		private void Load(object objResName)
		{
			// 资源名
			string strResName = objResName as string;

			// 不重复加载资源
			if (IsResLoaded(strResName))
			{
				return;
			}

			// 这里为了保存文件流，不使用using语句，在Release中手动释放资源
			FileStream fsResource = File.OpenRead (strResName);

			// 获取资源版本
			int nVersion = BitConverter.ToInt32 (ReadBytes (fsResource, 8), 0);

			// 获取资源头长度
			int nTitle = BitConverter.ToInt32 (ReadBytes (fsResource, 8), 0);

			// 读取所有文件信息
			byte[] byAllFileInfo = ReadBytes (fsResource, nTitle);

			// 读取资源头数据
			// nReadBytes已读取的字节 
			for (int nReadBytes = 0; nReadBytes < byAllFileInfo.Length; ) 			
			{
				// 文件数据
				tagFileObj tagObj = new tagFileObj();

				// 资源包名
				tagObj.strResName = strResName;

				// 文件流
				tagObj.fsResource = fsResource;

				// 读取一条文件信息的长度
				int nFileInfoLen = BitConverter.ToInt32( GetSubArray (byAllFileInfo, nReadBytes, 8), 0);
				nReadBytes += 8;

				// 读取一条文件信息
				byte[] byFileInfo = GetSubArray (byAllFileInfo, nReadBytes, nFileInfoLen);
				nReadBytes += nFileInfoLen;

				// 数组索引
				int nIndex = 0;

				// 文件名长度
				Int16 nNameLen = BitConverter.ToInt16 (byFileInfo, nIndex);
				nIndex += 2;

				// 文件名
				tagObj.strFileName = Encoding.UTF8.GetString (byFileInfo, nIndex, nNameLen);
				nIndex += nNameLen;

				// 文件长度
				tagObj.nLength = BitConverter.ToInt32 (byFileInfo, nIndex);
				nIndex += 4;

				// 文件开始位置
				tagObj.nBegin = BitConverter.ToInt64 (byFileInfo, nIndex);
				nIndex += 8;

				// 加到字典
				AddOneFileToCollections (tagObj, nVersion);
			}	

			lock (SynLoadState)
			{
				m_setLoadedRes.Add(strResName);
			}
		} 

		// 获取文件内容
		public byte[] GetBytesData(string strFileName, int nVersion, int nStartIndex = 0, int nLength = 0)
		{
			// 规范文件名
			FormatFileName (ref strFileName);
				
			// 如果字典里有这个文件
			tagFileKey tagKey = new tagFileKey(strFileName, nVersion);
			if (m_dtFileData.ContainsKey (tagKey)) 
			{
				// 读取的长度不超过文件本身的长度
				if (nStartIndex + nLength > m_dtFileData [tagKey].nLength || 0 == nLength) 
				{
					nLength = m_dtFileData [tagKey].nLength - nStartIndex;
				}

				// 跳转文件位置
				m_dtFileData [tagKey].fsResource.Seek ( m_dtFileData [tagKey].nBegin + nStartIndex, SeekOrigin.Begin);
				// 读取字节
				return ReadBytes (m_dtFileData [tagKey].fsResource, nLength);
			}

			return null;
		}

		// 资源是否加载完成
		public bool IsResLoaded(string strResName)
		{
			return m_setLoadedRes.Contains (strResName);
		}

		public List<tagFileObj> GetLoadResourceData ()
		{
			return m_lstFileObj;
		}

		// 读取字节流数据
		private byte[] ReadBytes(FileStream fsResource, int nCount)
		{
			if (fsResource == null)
			{
				return null;
			}

			// 读取数据
			byte[] byData = new byte[nCount];
			fsResource.Read (byData, 0, byData.Length);
			return byData;
		}

		// 获取数组的子数组
		private byte[] GetSubArray(byte[] bySrcArray, int nStartIndex, int nLength)
		{
			byte[] bySubArray = new byte[nLength];
			Array.ConstrainedCopy (bySrcArray, nStartIndex, bySubArray, 0, nLength);
			return bySubArray;
		}

		// 释放所有资源
		private void Release()
		{
			foreach (tagFileObj tagObj in m_dtFileData.Values) 
			{
				if (tagObj.fsResource != null && tagObj.fsResource.CanRead) 
				{
					tagObj.fsResource.Close ();
					tagObj.fsResource.Dispose ();
				}
			}
		}

		// 增加一个文件
		private void AddOneFileToCollections( tagFileObj tagObj, int nVersion)
		{
			// 如果字典里有这个文件了
			tagFileKey tagKey =  new tagFileKey(tagObj.strFileName, nVersion);
			if (m_dtFileData.ContainsKey (tagKey) )
			{
				tagFileObj tagExistObj = m_dtFileData [tagKey];

				// 比较资源加载顺序
				if (m_dtResLoadOrder [tagExistObj.strResName] > m_dtResLoadOrder[tagObj.strResName]) 
				{
					return;
				}
			}

			lock (SynDtRes) 
			{					
				// 保存资源数据 资源包不保存后缀
				m_dtFileData.Add (tagKey, tagObj);
				m_lstFileObj.Add (tagObj);
			}
		}

		// 格式化文件名
		private void FormatFileName(ref string strFileName)
		{
			// 不同操作系统下的目录索引可能不能，转换成'/'形式
			strFileName = strFileName.Replace ("\\", "/");

			// 如果查找文件时传入的参数是 ./image/123.png 
			// 但资源里放存的是 image/123.png 
			// 所以要去掉开头的./
			if (strFileName.IndexOf ("./") == 0)
			{
				strFileName = strFileName.Remove (0, 2);
			}

		}

	}
}

