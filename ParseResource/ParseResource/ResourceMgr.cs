﻿using System;
using System.Drawing;
using System.Media;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;

/// <summary>
/// /资源管理器
/// 负责资源的加载，及提供调用的接口
/// </summary>
namespace ParseResource
{
	
	public class ResourceMgr
	{
		// 单例
		private static object SynObject = new object ();
		private static ResourceMgr inst;
		public static ResourceMgr Inst
		{
			get 
			{
				lock (SynObject)
				{
					if (inst == null) 
					{
						inst = new ResourceMgr ();
					}
					return inst;
				}
			}
		}

		// 默认版本
		private int nDefaultVersion;
		public int DefaultVersion {
			get {
				return nDefaultVersion;
			}
			set {
				nDefaultVersion = value;
			}
		}

		public ResourceMgr ()
		{
		}

		// 获取文本
		public string GetText(string strFile, int nVersion = -1)
		{
			// 如果没有指定版本号，就用默认的版本号
			if (nVersion < 0) 
			{
				nVersion = nDefaultVersion;
			}

			return TextAdaptor.Inst.GetObject (strFile, nVersion) as string;
		}

		// 获取图片
		public Image GetImage(string strFile, int nVersion = -1)
		{
			// 如果没有指定版本号，就用默认的版本号
			if (nVersion < 0) 
			{
				nVersion = nDefaultVersion;
			}

			return ImageAdaptor.Inst.GetObject (strFile, nVersion) as Image;
		}

		// 获取音效
		public SoundPlayer GetSound(string strFile, int nVersion = -1)
		{
			// 如果没有指定版本号，就用默认的版本号
			if (nVersion < 0) 
			{
				nVersion = nDefaultVersion;
			}

			return SoundAdaptor.Inst.GetObject (strFile, nVersion) as SoundPlayer;
		}

		// 获取字节流
		public byte[] GetSrcData(string strFile, int nVersion = -1)
		{
			// 如果没有指定版本号，就用默认的版本号
			if (nVersion < 0) 
			{
				nVersion = nDefaultVersion;
			}

			return CommonFileAdaptor.Inst.GetObject (strFile, nVersion) as byte[];
		}

		// 加载资源，返回载是否成功
		// nWaitingSeconds等待秒数
		// nElapsedMilliseconds返回加载花费的时间
		public bool Loading(string strPath, ref long nElapsedMilliseconds, int nWaitingSeconds = 5)
		{
			// 资源包是否存在
			if(!IsExistFile(strPath))
			{
				return false;
			}

			// 加载资源
			ResourceData.Inst.StartLoadRes (strPath);

			// 获得计时器
			Stopwatch timer = new Stopwatch();
			// 开始计时
			timer.Start ();

			// 加载完成或者超过时间返回结果
			while(timer.ElapsedMilliseconds < nWaitingSeconds * 1000)
			{
				// 如果加载完成了，直接返回
				if (IsLoadingComplete(strPath)) 
				{
					nElapsedMilliseconds = timer.ElapsedMilliseconds;
					// 停止计时
					timer.Stop();
					return true;
				}
			}

			// 停止计时
			timer.Stop();
			return false;
		}

		// 获得加载的资源文件数据
		public List<tagFileObj> GetLoadResourceData ()
		{
			return ResourceData.Inst.GetLoadResourceData ();
		}

		// 是否加载完成
		private bool IsLoadingComplete(string strResource)
		{
			return ResourceData.Inst.IsResLoaded (strResource);
		}

		// 是否存在文件
		private bool IsExistFile(string strFileName)
		{
			return File.Exists (strFileName);
		}
	}
}

