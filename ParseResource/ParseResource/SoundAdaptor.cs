﻿using System;
using System.Media;
using System.IO;

namespace ParseResource
{
	/// <summary>
	/// 音效适配器，适配音效资源
	/// </summary>
	public class SoundAdaptor:FileAdaptor
	{
		// 单例
		private static SoundAdaptor inst;
		public static SoundAdaptor Inst
		{
			get 
			{
				lock (SynObject)
				{
					if (inst == null) 
					{
						inst = new SoundAdaptor ();
					}
					return inst;
				}
			}
		}

		public SoundAdaptor ()
		{
		}

		public override object GetObject(string strFileName, int nVersion)
		{
			byte[] byData = ResourceData.Inst.GetBytesData (strFileName, nVersion);

			// 判空
			if (byData == null) 
			{
				return null;
			}

			MemoryStream msData = new MemoryStream(byData);
			SoundPlayer spData = new SoundPlayer (msData);
			return spData;
		}
	}
}

